/***
*
* @brief		Logger
* @author		yun
* @date			2016-12-20 11:01:16
* @version		1.0.1
* @desc			
*
*/

#include	"platform_logger.h"

#include    <stdint.h>
#include 	<stdarg.h>

// 硬件相关函数
static void serial_init(void);
static int serial_send(unsigned char *buffer, int num);

//日志输出全局变量
platform_log_level_typedef		platform_log_level;

//
void platform_log_init(platform_log_level_typedef log_level){
	serial_init();
	platform_log_level = log_level;
}

//
int log_printf(const char *fmt, ...){
	char printf_buffer[256];
	va_list args;
	int printed = 0;
	va_start(args, fmt);
	printed = vsprintf(printf_buffer, fmt, args);
	if(printed > 256) return -1;
	va_end(args);
	serial_send((unsigned char *)printf_buffer, printed);
	return printed;
}

//
void platform_log_set_level(platform_log_level_typedef log_level){
	platform_log_level = log_level;
}

//
int platform_log_test(void){
	
	platform_log_init(PLATFORM_LOG_DEBUG);
	
	log_printf("logger: let's go!\n");
	
	return 0;
}


/**
* @level	Hardware
*/

#include 	"stm32f2xx.h"

#define     SERIAL_CLK          	RCC_APB2Periph_USART6
#define     SERIAL_CLK_INIT     	RCC_APB2PeriphClockCmd

#define     SERIAL_RX_PIN         		GPIO_Pin_7
#define     SERIAL_RX_SOURCE      		GPIO_PinSource7
#define     SERIAL_RX_GPIO_PORT   		GPIOC
#define     SERIAL_RX_GPIO_CLK    		RCC_AHB1Periph_GPIOC
#define     SERIAL_RX_AF          		GPIO_AF_USART6

#define     SERIAL_TX_PIN         		GPIO_Pin_6
#define     SERIAL_TX_SOURCE      		GPIO_PinSource6
#define     SERIAL_TX_GPIO_PORT   		GPIOC
#define     SERIAL_TX_GPIO_CLK    		RCC_AHB1Periph_GPIOC
#define     SERIAL_TX_AF          		GPIO_AF_USART6

#define     SERIAL_IRQn             	USART6_IRQn
#define     SERIAL	                	USART6
#define     SERIAL_IRQHandler       	USART6_IRQHandler

#define 	SERIAL_DR_Base        			((uint32_t)USART6 + 0x04)

#define     DMA_CLK_INIT                	RCC_AHB1Periph_DMA2
#define     SERIAL_RX_DMA_Stream       		DMA2_Stream1
#define     SERIAL_RX_DMA_Stream_IRQn  		DMA2_Stream1_IRQn
#define     SERIAL_RX_DMA_HTIF         		DMA_FLAG_HTIF5
#define     SERIAL_RX_DMA_TCIF         		DMA_FLAG_TCIF5


#define		UART_RX_BUF_SIZE		128

unsigned char logger_cmd_buffer[UART_RX_BUF_SIZE];


void serial_init(void){
    GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//串口空闲中断
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = SERIAL_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x1F;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//串口IO口
	RCC_AHB1PeriphClockCmd(SERIAL_RX_GPIO_CLK, ENABLE);
	SERIAL_CLK_INIT(SERIAL_CLK, ENABLE);
	
	GPIO_PinAFConfig(SERIAL_TX_GPIO_PORT, SERIAL_TX_SOURCE, SERIAL_TX_AF);
	GPIO_PinAFConfig(SERIAL_RX_GPIO_PORT, SERIAL_RX_SOURCE, SERIAL_RX_AF);
	//
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = SERIAL_TX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SERIAL_TX_GPIO_PORT, &GPIO_InitStructure);
	//
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Pin = SERIAL_RX_PIN;
	GPIO_Init(SERIAL_RX_GPIO_PORT, &GPIO_InitStructure);
	USART_DeInit(SERIAL);
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1 ;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(SERIAL, &USART_InitStructure);
	
	//串口DMA接收
	DMA_InitTypeDef DMA_InitStructure;
	RCC_AHB1PeriphClockCmd(DMA_CLK_INIT,ENABLE);
	DMA_ClearFlag(SERIAL_RX_DMA_Stream, SERIAL_RX_DMA_HTIF | SERIAL_RX_DMA_TCIF);
	//
	DMA_Cmd(SERIAL_RX_DMA_Stream, DISABLE);
	DMA_DeInit(SERIAL_RX_DMA_Stream);
	DMA_InitStructure.DMA_PeripheralBaseAddr = SERIAL_DR_Base;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;         
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_InitStructure.DMA_Channel = DMA_Channel_5;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)logger_cmd_buffer;
	DMA_InitStructure.DMA_BufferSize = (uint16_t)UART_RX_BUF_SIZE;
	DMA_Init(SERIAL_RX_DMA_Stream, &DMA_InitStructure);
	USART_DMACmd(SERIAL, USART_DMAReq_Rx, ENABLE);
	DMA_Cmd(SERIAL_RX_DMA_Stream, ENABLE);
	
	//串口接收
	USART_ITConfig(SERIAL, USART_IT_IDLE, ENABLE);
	USART_Cmd(SERIAL, ENABLE);
}


#include	"app_conf.h"

//串口空闲代表接收完成
void SERIAL_IRQHandler(void){
	if(USART_GetITStatus(SERIAL, USART_IT_IDLE) != RESET){
		short int i = SERIAL->SR;
		i = SERIAL->DR;
		USART_ClearITPendingBit(SERIAL, USART_IT_IDLE);
		DMA_Cmd(SERIAL_RX_DMA_Stream, DISABLE);
		//将数据拷贝出DMA缓冲区
		int len = UART_RX_BUF_SIZE - DMA_GetCurrDataCounter(SERIAL_RX_DMA_Stream);
		logger_cmd_buffer[len] = '\0';
        app_state_notification(APP_CMD);
		DMA_Cmd(SERIAL_RX_DMA_Stream, ENABLE);
	}
}

char * platform_get_cmd(void){
	return (char *) logger_cmd_buffer;
}

#include	"cmsis_os.h"

//串口发送
static int serial_send(unsigned char *buffer, int num){
	// 临时解决方案，关闭任务调度
	vTaskSuspendAll();
	while(num--){
		while(USART_GetFlagStatus(SERIAL, USART_FLAG_TC) != SET);
		USART_SendData(SERIAL, *buffer++);
	}
	xTaskResumeAll();
	return num;
}
//

int fputc(int ch, FILE *f){
	if (ch == '\n')  {
		while (USART_GetFlagStatus(SERIAL, USART_FLAG_TXE) == RESET);
        USART_SendData(SERIAL, 0x0D);
    }
	while (USART_GetFlagStatus(SERIAL, USART_FLAG_TXE) == RESET);
    USART_SendData(SERIAL, (uint8_t) ch);
    return ch;
}


