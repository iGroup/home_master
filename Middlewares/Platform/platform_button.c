/**
*
* @brief	AirKiss 按键
* @author	yun
* @date		2016-12-23
* @desc		AirKiss 按键
* @version
*
*/

#include	"platform_button.h"

#include	"platform_logger.h"
#include	"stm32f2xx.h"
#include	"mxchipWNet.h"

#define 	AIRKISS_BTN_CLK_INIT			RCC_AHB1PeriphClockCmd
#define 	AIRKISS_BTN_PIN         	    GPIO_Pin_1
#define		AIRKISS_BTN_Source				GPIO_PinSource1
#define 	AIRKISS_BTN_PORT   				GPIOA
#define 	AIRKISS_BTN_CLK    				RCC_AHB1Periph_GPIOA

//
#define		AIRKISS_BTN_EXTI_Line			EXTI_Line1
#define		AIRKISS_BTN_EXTI_Handler		EXTI1_IRQHandler
#define		AIRKISS_BTN_EXTI_IRQn			EXTI1_IRQn

platform_airkiss_callback	airkiss_callback = NULL;

int platform_airkiss_btn(platform_airkiss_callback callback){
	GPIO_InitTypeDef   GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(AIRKISS_BTN_CLK, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = AIRKISS_BTN_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;			
	GPIO_Init(AIRKISS_BTN_PORT, &GPIO_InitStructure);
	
	GPIO_PinAFConfig(AIRKISS_BTN_PORT, AIRKISS_BTN_Source, GPIO_AF_TRACE);
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = AIRKISS_BTN_EXTI_Line;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = AIRKISS_BTN_EXTI_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0X2C;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	airkiss_callback =  callback;
	
	return 0;
}


int platform_button_init(void){
	return 0;
}

static int airkiss_flag = 0;


void  AIRKISS_BTN_EXTI_Handler(void){
	if(EXTI_GetITStatus(AIRKISS_BTN_EXTI_Line) != RESET){
		if(airkiss_callback != NULL){
			airkiss_callback();
		}
		airkiss_flag = 1;
		EXTI_ClearFlag(AIRKISS_BTN_EXTI_Line);
		EXTI_ClearITPendingBit(AIRKISS_BTN_EXTI_Line);
	}
}

int platform_button_test(void){
	log_printf("let's go");
	platform_button_init();
	
	while(1){
		if(airkiss_flag == 1){
			airkiss_flag = 0;
			log_printf("airkiss\n");
		}
	}

	return 0;
}






