/**
*
* @brief	温湿度传感器数据测量
* @author	yun
* @date		2016-12-27
* @desc		温湿度传感器AM2303
* @version	v0.1
*
*/

#ifndef		__PLATFORM_TEMP_H
#define		__PLATFORM_TEMP_H

// 
// 温湿度测量回调函数指针声明
// t: 温度
// h: 湿度
// err: 错误
//
typedef int (*platform_temp_callback)(float h, float t, int err);

//
// 开始温度单次测量
// callback: 测量完成回掉函数
//
int platform_temp_start_measure(platform_temp_callback callback);

// 停止温湿度测量
int platform_temp_stop_measure(void);

// 测试函数
int platform_temp_test(void);

#endif

