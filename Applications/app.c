/**
*
* @brief	家庭控制中心(Home Master)
* @author	yun
* @date		2016-12-21
* @desc
* @version
*
*/

#include	"mxchipWNET.h"

#include	"platform_logger.h"
#include	"platform_calendar.h"
#include	"platform_433module.h"
#include	"platform_button.h"
#include	"platform_temp.h"

#include	"cmsis_os.h"

#include	"app_conf.h"
#include	"app_message.h"

#include	"service_device.h"
#include	"service_network.h"
#include	"service_server.h"
#include	"service_temp.h"
#include	"service_switch.h"
#include	"service_monitor.h"


// APP 信息
osMessageQId app_state_message = NULL;

// APP 状态
app_state_typedef volatile app_state;

// 应用状态管理线程
static void thread_manager(const void *argument);

// 应用状态处理函数声明
typedef		int (*app_state_handler)(void);

// APP 状态切换对应的函数
int app_state_network_discon(void);
int app_state_network_conn(void);
int app_state_server_discon(void);
int app_state_server_conn(void);
int app_state_server_login(void);
int app_state_server_logout(void);
int app_state_command(void);
int app_state_update(void);
int app_state_error(void);

// 
static const app_state_handler app_state_handler_table[] = {
	[APP_NETWORK_DISCON] =  app_state_network_discon,
	[APP_NETWORK_CON] =  app_state_network_conn,
	[APP_SERVER_DISCON] = app_state_server_discon,
	[APP_SERVER_CON] = app_state_server_conn,
	[APP_SERVER_LOGIN] = app_state_server_login,
	[APP_SERVER_LOGOUT] = app_state_server_logout,
	[APP_CMD] = app_state_command,
	[APP_UPDATE] = app_state_update,
	[APP_ERR] = app_state_error
};

//
int main(void){
	mxchipInit();
	platform_calendar_init();
	platform_log_init(PLATFORM_LOG_ERROR);
	
	//platform_module433_test();
	//platform_button_test();
	//platform_temp_test();
	//message_test();
	//service_device_test();

	log_printf("\n\n");
	log_notice_printf("Device: %s", DEVICE_NAME);
	
	osThreadDef(Root, thread_manager, osPriorityBelowNormal, 0, 256);
	osThreadCreate(osThread(Root), NULL);
	
	osKernelStart();
	while(1);
}

//
// 应用状态管理线程
// 
static void thread_manager(const void *argument){
	log_info_printf("thread: manager");
	
	app_state_message = xQueueCreate(1, sizeof(app_state_typedef));
	if(app_state_message == NULL){
		log_error_printf("app: message create error");
	}
	
	app_server_init();
	network_establish();
	service_temp_establish();
	service_switch_establish();
	
	while(1){
		osEvent app_state = osMessageGet(app_state_message, 2000); 
		if(app_state.status == osEventMessage){
			int event = app_state.value.v;
			// 查找对应的处理函数进行处理
			if(event < APP_STATE_MAX){
				app_state_handler hander =  app_state_handler_table[event];
				hander();
			}
		}
	}
}


//
// 网络断开连接
//
int app_state_network_discon(void){
	app_state = APP_NETWORK_DISCON;
	log_notice_printf("app: network disconnected");
	app_server_destroy();
	return 0;
}

//
// 网络连接成功
//
int app_state_network_conn(void){
	app_state = APP_NETWORK_CON;
	log_notice_printf("app: network connected");
	app_server_establish();
	return 0;
}


//
// 与服务器断开
//
int app_state_server_discon(void){
	app_state = APP_SERVER_DISCON;
	log_notice_printf("app: server disconnected");
	service_device_destroy();
	return 0;
}


//
// 与服务器连接成功
//
int app_state_server_conn(void){
	app_state = APP_SERVER_CON;
	log_notice_printf("app: server connected");
	service_device_establish();
	return 0;
}


//
// 设备登陆成功
//
int app_state_server_login(void){
	app_state = APP_SERVER_LOGIN;
	log_notice_printf("app: device login");
	
	// 上报设备信息
	switch_state_query();
	service_temp_query();
	
	return 0;
}


//
// 服务器登出
//
int app_state_server_logout(void){
	app_state = APP_SERVER_LOGOUT;
	return 0;
}


//
// 串口指令输入
//
int app_state_command(void){
	char * cmd = platform_get_cmd(); 
	log_notice_printf("cmd: %s", cmd);
	
	// 开关控制命令
	char * data = NULL;
	data = strstr(cmd, "switch:");
	if (data != NULL) {
		return switch_operate_cmd(cmd + strlen("switch:"));
	}
	
	// 监控命令
	data = strstr(cmd, "monitor:");
	if (data != NULL) {
		return service_monitor_cmd(cmd + strlen("monitor:"));
	}

	if (strcmp(cmd, "reboot") == 0 ){
		system_reload();
		return 0;
	}		
	
	return 0;
}


//
// 固件升级
//
int app_state_update(void){
	app_state = APP_UPDATE;
	return 0;
}

//
// 程序运行错误处理: 重启
//
int app_state_error(void){
	app_state = APP_ERR;
	log_error_printf("app run err, restart ...");
	system_reload();
	return 0;
}


//
// APP 状态通知
//
int app_state_notification(app_state_typedef state){
	if(osMessagePut(app_state_message, state, 100) == osOK){
		return 0;
	}
	return -1;
}


















