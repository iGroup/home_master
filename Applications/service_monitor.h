/**
*
* @brief	系统运行信息检测统计服务
* @author	yun
* @date		2017-02-07
* @desc		统计FreeRTOS运行堆栈，CPU利用率等信息
* 			管理日志输出功能
* @version	v0.1
*
*/

#ifndef 	__SERVICE_MONITOR_H
#define		__SERVICE_MONITOR_H

int service_monitor_cmd(char * cmd);

#endif

