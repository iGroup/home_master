/**
*
* @brief	设备基本服务
* @author	yun
* @date		2017-01-20
*
* @desc		设备基本服务 [登入|登出|心跳]
* @version	v0.9
*
*/

#include	"service_device.h"

#include	"app_conf.h"
#include	"platform_logger.h"
#include	"platform_calendar.h"

#include	"service_server.h"

#include	"cmsis_os.h"

#include	<string.h>
#include	<stdio.h>

// 设备服务
#define		SERVICE_DEV_ID					"dev"
#define		SERVICE_DEV_OP_LOGIN			"login"
#define		SERVICE_DEV_OP_HB				"hb"
#define		SERVICE_DEV_OP_LOGOUT			"logout"


// 登陆数据格式
#define		SERVICE_DEV_OP_LOGIN_FORMAT			\
"{\"type\":\"login\",\"data\":{\"id\":\"%s\",\"key\":\"%s\",\"version\":\"%s\",\"time\":\"%s\"}}"


// 心跳数据格式
#define		SERVICE_DEV_OP_HB_FORMAT		\
"{\"type\":\"%s\",\"data\":\"%s\"}"


// 登出数据格式
#define		SERVICE_DEV_OP_LOGOUT_FORMAT		\
"{\"type\":\"logout\",\"data\":{\"id\":\"%s\",\"key\":\"%s\",\"time\":\"%s\"}}"


// 设备心跳线程
osThreadId		thread_heartbeat_id = NULL;

// 请求返回结果处理
static int service_device_login_handle(cJSON *post);
static int service_device_hb_handle(cJSON *post);

// 设备心跳子服务的建立与销毁
int service_dev_hb_establish(void);
int service_dev_hb_destroy(void);

// 心跳线程
static void thread_hb(void const *argument);

//
// 设备服务返回结果
// 返回结果: json数据格式
// 登陆成功返回结果: {"type":"login","state":"ok","time":"2017-01-20 15:42:05"}
// 登陆失败返回结果: {"type":"login","state":"fail","time":"2017-01-20 15:42:05"}
// 心跳报文返回结果: {"type":"hb","data":"2017-01-16 11:06:33"}
//
static int service_device_callback(char * sev, cJSON *post){
	if (post == NULL){
		return -1;
	}
	
	// type
	cJSON * type = cJSON_GetObjectItem(post, "type");
	if (type == NULL || type->type != cJSON_String) {
		return -1;
	}
	
	if (strcmp(type->valuestring, SERVICE_DEV_OP_HB) == 0){
		return service_device_hb_handle(post);
	}	
	
	if(strcmp(type->valuestring, SERVICE_DEV_OP_LOGIN) == 0){
		return service_device_login_handle(post);
	}		
	
	return -1;
}


//
// 设备登陆服务返回结果
//
static int service_device_login_handle(cJSON *post){
	// state
	cJSON * state = cJSON_GetObjectItem(post, "state");
	if (state == NULL || state->type != cJSON_String){
		return -1;
	}
	
	// time
	cJSON * time = cJSON_GetObjectItem(post, "time");
	if (time == NULL || time->type != cJSON_String){
		return -1;
	}
	
	// login result
	if(strcmp(state->valuestring, "ok") == 0){
		app_state_notification(APP_SERVER_LOGIN);
		if(platform_calendar_set(time->valuestring) == 0){
			log_notice_printf("login time: %s", time->valuestring);
		}
		// 设备登陆成功，则建立心跳自服务
		service_dev_hb_establish();
	}
	
	return 0;
}

//
// 设备心跳服务返回结果
//
static int service_device_hb_handle(cJSON *post){
	cJSON * data = cJSON_GetObjectItem(post, "data");
	if(data == NULL || data->type != cJSON_Number){
		return -1;
	}
	return 0;
}

//
//  设备心跳维护线程
//
static void thread_hb(void const *argument) {
	log_info_printf("thread: hb");
	
	char heartbeat[40] = { 0 };
	char data[20] = { 0 };
	while(1){
		platform_calendar_get(data);
		sprintf(heartbeat, SERVICE_DEV_OP_HB_FORMAT, \
			SERVICE_DEV_OP_HB, data);
		server_send_data(SERVICE_DEV_ID, heartbeat);
		osDelay(SEV_DEV_HB_INTERVAL);
	}
}

// 设备心跳子服务的建立
int service_dev_hb_establish(void){
	osThreadDef(Heartbeat, thread_hb, osPriorityBelowNormal, 0, 256);
	thread_heartbeat_id = osThreadCreate(osThread(Heartbeat), NULL);
	if(thread_heartbeat_id == NULL){
		log_error_printf("service: thread heartbeat create");
		app_state_notification(APP_ERR);
		return -1;
	}
	return 0;
}

// 设备心跳子服务的销毁
int service_dev_hb_destroy(void){
	if(thread_heartbeat_id != NULL){
		osThreadTerminate(thread_heartbeat_id);
		osDelay(10);
	}
	return 0;
}

//
// 登陆服务创建
//
int service_device_establish(void){
    char operate[180];
	char time[20];
	platform_calendar_get(time);
	sprintf(operate, SERVICE_DEV_OP_LOGIN_FORMAT, \
			DEVICE_ID,	DEVICE_KEY, APP_VERSION, "2016-12-22 13:28:54");
	if(server_send_data(SERVICE_DEV_ID, operate) == 0){
		// 监听登陆返回结果
		server_post_subscribe(SERVICE_DEV_ID, service_device_callback);
		return 0;
	}
	return -1;
}

//
// 设备服务销毁
//
int service_device_destroy(void){
	service_dev_hb_destroy();
	return 0;
}


//
// 设备服务测试
//
int service_device_test(void){
	char operate[180];
	
	log_info_printf("test: service device !");
	
	sprintf(operate, SERVICE_DEV_OP_LOGIN_FORMAT, \
	    DEVICE_ID,	DEVICE_KEY, APP_VERSION, "2016-12-22 13:28:54");
	
	log_info_printf("msg: %s", operate);
	
	return 0;
}




